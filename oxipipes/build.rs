use std::fs;
use chrono::{Datelike, Timelike, Local};
use rust_utils::utils;

const MONTHS: [&str; 12] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    let date = Local::now().date();
    let month = MONTHS[date.month() as usize - 1];
    let day = date.day();
    let year = date.year();
    let date_str = format!("{day} {month} {year}");
    let date_version = utils::run_command("git", false, ["show", "-s", "--format=%cd", "--date=format:%Y.%m.%d", "-1"]).output;
    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second(); 
    let time_str = format!("{hour:02}:{minute:02}:{second:02}");
    let manpage = format!(
        ".\\\" Manpage for oxipipes (part of The Rust Toys).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH oxipipes 1 \"{date_str}\" \"{date_version}\" \"oxipipes man page\"\n\
        .SH NAME\n\
        oxipipes [options] \\- command-line pipes screensaver\n\
        .SH SYNOPSIS\n\
        oxipipes -c <color>\n\
        .br\n\
        oxipipes -p <max number of pipes>\n\
        .br\n\
        oxipipes -s <set number>\n\n\
        .SH DESCRIPTION\n\
        Command-line pipes screensaver similar to pipes.sh but in Rust instead.\n\n\
        .SH OPTIONS\n\
        Use -p <number> to limit the amount of pipes to a certain number\n\
        .br\n\n\
        .br\n\
        Use -c to specify the colors of the pipes. Multiple colors can be used, separated by commas.\n\
        .br\n\
        Valid colors are:\n\
        .br\n    \
        gray, gs, or grayscale: Random grayscale colors\n\
        .br\n    \
        multi: Random colors\n\
        .br\n    \
        multib: Random basic colors\n\
        .br\n    \
        black\n\
        .br\n    \
        blue\n\
        .br\n    \
        cyan\n\
        .br\n    \
        green\n\
        .br\n    \
        magenta\n\
        .br\n    \
        red\n\
        .br\n    \
        white\n\
        .br\n    \
        yellow\n\
        .br\n    \
        Any number from 0-255 representing a 8-bit color\n\
        .br\n    \
        Any hex number representing a 24-bit color\n\
        .br\n\n\
        .br\n\
        Use -s <number> to specify the type of pipe to use (valid numbers are 0-7). Multiple numbers separated by commas be used to specify multiple sets. Specify multi to use random sets.\n\n\
        .SH EXAMPLES\n\
        oxipipes -c red,white,blue\n\
        .br\n\
        oxipipes -c gs -s 1,2,5\n\n\
        .SH SEE ALSO\n\
        oximatrix(1)\n\
        .br\n\
        gol(1)\n\
        .br\n\
        2048(1)\n\
        .br\n\
        lant(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)");
    fs::create_dir("../man").unwrap_or(());
    fs::remove_file("../man/oxipipes.1").unwrap_or(());
    fs::write("../man/oxipipes.1", manpage).unwrap();
}