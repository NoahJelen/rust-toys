use cursive::{
    Printer,
    traits::View,
    Vec2
};
use rand::{Rng, thread_rng};

use crate::pipes;

pub struct PipesView {
    width: usize,
    height: usize,
    pipes: Vec<pipes::Pipe>,
    max_pipes: Option<usize>,
    colors: Vec<String>,
    sets: Vec<usize>
}

impl PipesView {
    pub fn new(max_pipes: Option<usize>, colors: Vec<String>, sets: Vec<usize>) -> PipesView {
        PipesView {
            width: 0,
            height: 0,
            pipes: Vec::new(),
            max_pipes,
            colors,
            sets
        }
    }

    pub fn clear(&mut self) { self.pipes = Vec::new(); }

    pub fn update(&mut self) {
        let max_pipes = match self.max_pipes {
            Some(num) => num,
            None => self.width / 2
        };
        let mut random = thread_rng();

        for pipe in self.pipes.iter_mut().rev() {
            if random.gen_range(0..10) == 0 {
                pipe.change_dir();
            }
            pipe.extend();
        }

        if self.pipes.len() < max_pipes {
            let color = pipes::get_color(&self.colors[random.gen_range(0..self.colors.len())]);
            let new_pipe = pipes::Pipe::new(color, self.width, self.height, self.sets[random.gen_range(0..self.sets.len())]);
            self.pipes.push(new_pipe);
        }

        if self.pipes.len() >= max_pipes {
            self.pipes.retain(|pipe| !pipe.is_finished());
        }

        self.pipes.retain(|pipe| !pipe.is_finished() || pipe.num_segments() > 3);
        //self.pipes = pipes;
    }
}

impl View for PipesView {
    fn draw(&self, printer: &Printer) {
        for pipe in &self.pipes {
            pipe.draw(printer);
        }
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x;
        self.height = constraint.y;
        Vec2::new(constraint.x, constraint.y)
    }
}