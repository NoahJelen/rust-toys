use cursive::{
    Printer,
    theme::{BaseColor, Color, ColorStyle}
};
use rand::{Rng, thread_rng, seq::SliceRandom};

const SETS: [&str; 8] = [
    "━┃┗┛┏┓",
    "─│╰╯╭╮",
    "─│└┘┌┐",
    "═║╚╝╔╗",
    "╾╿┕┙┎┑",
    "-|++++",
    "-|\\//\\",
    "██████"
];

const DIRECTIONS: [Direction; 4] = [
    Direction::North,
    Direction::South,
    Direction::East,
    Direction::West
];

#[derive(Copy, Clone, PartialEq)]
enum Direction {
    North,
    South,
    East,
    West
}

impl Direction {
    pub fn is_reverse(&self, new_dir: Direction) -> bool {
        (*self == Direction::North && new_dir == Direction::South)
        || (*self == Direction::South && new_dir == Direction::North)
        || (*self == Direction::East && new_dir == Direction::West)
        || (*self == Direction::West && new_dir == Direction::East)
    }
}

#[derive(Clone, Copy)]
struct Segment {
    sym: char,
    x: usize,
    y: usize,
    dir: Direction
}

impl Segment {
    fn new(x: usize, y: usize, dir: Direction) -> Segment {
        Segment {
            x,
            y,
            sym: '█',
            dir
        }
    }

    #[allow(clippy::iter_nth_zero)]
    fn is_straight(&self) -> bool {
        for set in SETS {
            if self.sym == set.chars().nth(0).unwrap() || self.sym == set.chars().nth(1).unwrap() {
                return true;
            }
        }
        false
    }
}

pub struct Pipe {
    color: Color,
    segments: Vec<Segment>,
    cur_dir: Direction,
    max_x: usize,
    max_y: usize,
    set: &'static str
}

impl Pipe {
    pub fn new(color: Color, max_x: usize, max_y: usize, mut set: usize) -> Pipe {
        let mut random = thread_rng();
        let cur_dir = *DIRECTIONS.choose(&mut random).unwrap();
        if set > 7 {
            set = 7;
        }
        Pipe {
            color,
            segments: Vec::new(),
            cur_dir,
            max_x,
            max_y,
            set: SETS[set]
        }
    }
    
    pub fn num_segments(&self) -> usize {
        self.segments.len()
    }

    #[allow(clippy::iter_nth_zero)]
    pub fn extend(&mut self) {
        if self.is_finished() {
            return;
        }
        let mut random = thread_rng();

        // if the segments vector is empty, add a new segment
        if self.segments.is_empty() {
            match self.cur_dir {
                Direction::North => self.segments.push(Segment::new(random.gen_range(0..self.max_x),self.max_y,self.cur_dir)),
                Direction::South => self.segments.push(Segment::new(random.gen_range(0..self.max_x),0,self.cur_dir)),
                Direction::West => self.segments.push(Segment::new(random.gen_range(0..self.max_x),self.max_x,self.cur_dir)),
                Direction::East => self.segments.push(Segment::new(random.gen_range(0..self.max_x),0,self.cur_dir))
            }
        }

        let mut x = self.segments.last().unwrap().x;
        let mut y = self.segments.last().unwrap().y;

        match self.cur_dir {
            Direction::North => {
                if y == 0 {
                    return;
                }
                y-=1
            },
            Direction::South => y += 1,
            Direction::East => x += 1,
            Direction::West => {
                if x == 0 {
                    return;
                }
                x -= 1
            }
        }

        if x > self.max_x || y > self.max_y {
            return;
        }

        self.segments.push(Segment::new(x, y, self.cur_dir));
        for segment in self.segments.iter_mut() {
            match segment.dir {
                Direction::North | Direction::South=> segment.sym = self.set.chars().nth(1).unwrap(),
                Direction::East | Direction::West => segment.sym = self.set.chars().nth(0).unwrap()
            }
        }
        if self.segments.len() <= 3 {
            return;
        }
        let mut segments_test = self.segments.clone();
        for (i, segment) in self.segments.iter_mut().enumerate() {
            if i > 0 && i < segments_test.len() - 1 && segments_test[i - 1].is_straight() {
                match (segments_test[i - 1].dir, segments_test[i + 1].dir) {
                    (Direction::East, Direction::South) | (Direction::North, Direction::West) => {
                        segment.sym = self.set.chars().nth(5).unwrap();
                        segments_test[i].sym = self.set.chars().nth(5).unwrap();
                    },
                    (Direction::East, Direction::North) | (Direction::South, Direction::West) => {
                        segment.sym = self.set.chars().nth(3).unwrap();
                        segments_test[i].sym = self.set.chars().nth(3).unwrap();
                    },
                    (Direction::West, Direction::North) | (Direction::South,Direction::East) => {
                        segment.sym = self.set.chars().nth(2).unwrap();
                        segments_test[i].sym = self.set.chars().nth(2).unwrap();
                    },
                    (Direction::West, Direction::South) | (Direction::North,Direction::East) => {
                        segment.sym = self.set.chars().nth(4).unwrap();
                        segments_test[i].sym = self.set.chars().nth(4).unwrap();
                    },
                    _ => { }
                }
            }
        }
    }

    pub fn change_dir(&mut self) {
        if self.segments.len() < 3 { return; }

        let cur_dir = self.cur_dir;
        let last_elem = self.segments.len() - 1;
        if self.segments[last_elem].dir == cur_dir && self.segments[last_elem- 1].dir == cur_dir && self.segments[last_elem - 2].dir == cur_dir {
            let mut random = thread_rng();
            let new_dir = *DIRECTIONS.choose(&mut random).unwrap();
            if self.cur_dir.is_reverse(new_dir) { return; }
            self.cur_dir = new_dir;
        };
    }

    pub fn is_finished(&self) -> bool {
        if self.segments.is_empty() { return false; }

        (
            self.segments.first().unwrap().x >= self.max_x
            || self.segments.first().unwrap().y >= self.max_y
            || self.segments.first().unwrap().x == 0
            || self.segments.first().unwrap().y == 0
        )
        && (
            self.segments.last().unwrap().x >= self.max_x
            || self.segments.last().unwrap().y >= self.max_y
            || self.segments.last().unwrap().x == 0
            || self.segments.last().unwrap().y == 0
        )
    }

    pub fn draw(&self, printer: &Printer) {
        for segment in &self.segments {
            let style = ColorStyle::new(self.color,Color::TerminalDefault);
            printer.with_color(style, |printer| printer.print((segment.x,segment.y),&segment.sym.to_string()));
        }
    }
}

pub fn get_color(color_str: &str) -> Color {
    let basic_colors = [BaseColor::Black, BaseColor::Blue, BaseColor::Cyan, BaseColor::Green, BaseColor::Magenta, BaseColor::Red, BaseColor::White, BaseColor::Yellow];
    let mut random =  thread_rng();
    let mut basecolor =  BaseColor::Black;
    let mut is_base = true;
    let is_dark: bool = random.gen();

    // is the color a named color?
    match color_str.to_uppercase().as_str() {
        "GRAY" | "GS" | "GRAYSCALE" => {
            let a: u8 = random.gen();
            return Color::Rgb(a,a,a);
        },
        "MULTI" => return Color::Rgb(random.gen(),random.gen(),random.gen()),
        "MULTIB" => basecolor = basic_colors[random.gen_range(0..8)],
        "BLACK" => basecolor = BaseColor::Black,
        "BLUE" => basecolor = BaseColor::Blue,
        "CYAN" => basecolor = BaseColor::Cyan,
        "GREEN" => basecolor = BaseColor::Green,
        "MAGENTA" => basecolor = BaseColor::Magenta,
        "RED" => basecolor = BaseColor::Red,
        "WHITE" => basecolor = BaseColor::White,
        "YELLOW" => basecolor = BaseColor::Yellow,
        _ => is_base = false
    }

    if is_base {
        return if is_dark {
            Color::Dark(basecolor)
        }
        else {
            Color::Light(basecolor)
        }
    }

    // is it a number (0-255)?
    if let Ok(num) = color_str.parse::<u8>() {
        return Color::from_256colors(num);
    }

    // is it a hex string?
    if let Ok(cols) = hex::decode(color_str) {
        return Color::Rgb(cols[0], cols[1], cols[2])
    }
    
    Color::TerminalDefault
}