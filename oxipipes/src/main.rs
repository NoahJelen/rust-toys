use std::{env, process};
use cursive::{
    event,
    view::Nameable,
    views::ResizedView,
    theme::{BaseColor, Color, PaletteColor, Theme}
};

mod view;
mod pipes;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut num_pipes: isize = -1;
    let mut colors: Vec<String> =  vec!["MULTI".to_string()];
    let mut sets: Vec<usize> = vec![0];
    for (i, arg) in args.iter().enumerate() {
        match arg.as_str() {
            "--color" | "-c" => {
                if i >= (args.len() - 1) {
                    eprintln!("Please specify a color!");
                    return;
                }
                let c = &args[i + 1];
                let color_vec: Vec<String> = c.split(',').map(|color| color.to_string()).collect();
    
                for color in color_vec {
                    if pipes::get_color(&color.to_string()) == Color::TerminalDefault {
                        eprintln!("Invalid color: {color}");
                        println!("Examples of valid colors:");
                        println!("The 8 TTY colors: black, blue, cyan, green, magenta, red, white, and yellow");
                        println!("0-255 representing any of the 256 colors");
                        println!("or a 6 digit hex code representing a color");
                        return;
                    }
                }
                colors = args[i + 1]
                    .split(',')
                    .map(|color_str| color_str.to_string())
                    .collect();
            },

            "--pipes" | "-p" => {
                if i >= (args.len() - 1) {
                    eprintln!("Invalid number!");
                    return;
                }
                else {
                    num_pipes = match args[i + 1].parse::<isize>() {
                        Ok(num) => num,
                        Err(_) => {
                            eprintln!("Invalid number!");
                            return;
                        }
                    }
                }
            },

            "--sets" | "-s" => {
                if i >= (args.len() - 1) {
                    eprintln!("Please specify a set number!");
                    return;
                }
                let s = &args[i + 1];
                if s.to_uppercase() == "MULTI" {
                    sets = vec![0, 1, 2, 3, 4, 5, 6, 7];
                }
                else {
                    sets = s.split(',').map(|set| {
                        match set.to_string().parse::<usize>() {
                            Ok(num) => num,
                            Err(_) => {
                                eprintln!("Invalid set number!");
                                process::exit(1);
                            }
                        }
                    }).collect();
                }
            },

            _ => { }
        }
    }

    let mut theme = Theme::default();
    theme.palette[PaletteColor::Background] = Color::TerminalDefault;
    theme.palette[PaletteColor::View] = Color::TerminalDefault;
    theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
    let mut root = cursive::default();
    root.set_theme(theme);
    if num_pipes < -1 {
        num_pipes = -num_pipes;
    }
    let pipes = match num_pipes {
        -1 => None,
        _ => Some(num_pipes as usize)
    };
    root.add_fullscreen_layer(ResizedView::with_full_screen(view::PipesView::new(pipes,colors,sets).with_name("pipes")));

    root.set_global_callback(event::Event::WindowResize, |view| {
        view.call_on_name("pipes", |pipes: &mut view::PipesView| pipes.clear());
    });

    root.set_global_callback(event::Event::Refresh, |view| {
        view.call_on_name("pipes", |pipes: &mut view::PipesView| pipes.update());
    });

    root.set_global_callback('q', |view| view.quit());

    root.set_global_callback('c', |view| {
        view.call_on_name("pipes", |pipes: &mut view::PipesView| pipes.clear());
    });

    root.set_fps(30);
    root.run();
}