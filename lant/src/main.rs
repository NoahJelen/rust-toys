use cursive::{
    event::*,
    theme::{Theme, PaletteColor, Color, BaseColor},
    views::ResizedView,view::Nameable
};
use std::env;

mod view;
mod game;

fn main() {
    let mut rules = "RL";
    let mut num_ants: usize = 1;
    let mut manual = false;
    let args: Vec<String> = env::args().collect();
    for (i, arg) in args.iter().enumerate() {
        match arg.as_str() {
            "--rules" | "-r" => {
                if i >= (args.len() - 1) {
                    println!("Please specify the rules!");
                    return;
                }
                rules = &args[i + 1];
            },

            "--ants" | "-a" => {
                if i >= (args.len() - 1) {
                    println!("Please specify the number of ants!");
                    return;
                }
                num_ants = match args[i + 1].parse::<usize>() {
                    Ok(num) => num,
                    Err(_) => {
                        eprintln!("Invalid number!");
                        return;
                    }
                }
            },

            "--manual" | "-m" => manual = true,

            _ => { }
        }
    }

    let mut theme = Theme::default();
    theme.palette[PaletteColor::Background] = Color::TerminalDefault;
    theme.palette[PaletteColor::View] = Color::TerminalDefault;
    theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
    let mut root = cursive::default();
    root.set_theme(theme);
    root.add_fullscreen_layer(ResizedView::with_full_screen(view::LangtonAntView::new(rules, num_ants).with_name("lant")));

    root.add_global_callback(Event::Refresh, move |view| {
        if !manual {
            view.call_on_name("lant", |lant: &mut view::LangtonAntView| lant.update());
        }
    });

    root.add_global_callback(Event::Key(Key::Enter), move |view| {
        if manual {
            view.call_on_name("lant", |lant: &mut view::LangtonAntView| lant.update());
        }
    });

    root.add_global_callback('q', |view| view.quit());
    root.set_fps(30);
    root.run();
}