use cursive::theme::Color;
use std::cmp::PartialEq;

mod board;
mod ant;
mod cell;

pub use {board::*, ant::*, cell::*};

#[derive(Copy, Clone)]
pub enum Direction {
    East,
    West,
    North,
    South
}

impl Direction {
    pub fn apply_rule(&self, rule: Rule) -> Direction {
        match rule {
            Rule::Right(_) => self.right(),
            Rule::Left(_) => self.left(),
            Rule::Reverse(_) => self.reverse(),
            Rule::Straight(_) => *self
        }
    }

    fn left(&self) -> Direction {
        match *self {
            Direction::East => Direction::North,
            Direction::West => Direction::South,
            Direction::North => Direction::West,
            Direction::South => Direction::East
        }
    }

    fn right(&self) -> Direction {
        match *self {
            Direction::East => Direction::South,
            Direction::West => Direction::North,
            Direction::North => Direction::East,
            Direction::South => Direction::South
        }
    }

    fn reverse(&self) -> Direction {
        match *self {
            Direction::East => Direction::West,
            Direction::West => Direction::East,
            Direction::North => Direction::South,
            Direction::South => Direction::North
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Rule {
    Right(Color),
    Left(Color),
    Straight(Color),
    Reverse(Color)
}

impl Rule {
    pub fn get_color(&self) -> Color {
        match *self {
            Rule::Right(color) | Rule::Left(color) | Rule::Straight(color) | Rule::Reverse(color) => color,
        }
    }
}