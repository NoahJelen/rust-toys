use cursive::{Printer, theme::ColorStyle};
use super::*;

pub struct Board {
    pub ants: Vec<Ant>,
    pub rules: Vec<Rule>,
    cells: Vec<Vec<Cell>>,
    size: (usize, usize)
}

impl Board {
    pub fn new(ants: Vec<Ant>, default_rule: Rule, size_x: usize, size_y: usize, rules: Vec<Rule>) -> Board {
        Board {
            ants,
            rules,
            cells: vec![vec![Cell::new(default_rule); size_x]; size_y],
            size: (size_x, size_y)
        }
    }

    pub fn tick(&mut self) {
        for ant in &mut self.ants {
            let cell = &mut self.cells[ant.pos.1][ant.pos.0];
            ant.advance(self.size.0, self.size.1, cell.rule);

            cell.rule_idx += 1;
            if cell.rule_idx >= self.rules.len() {
                cell.rule_idx = 0;
            }
    
            cell.rule = self.rules[cell.rule_idx];
        }
    }

    pub fn print(&self, printer: &Printer) {
        for (r, rows) in self.cells.iter().enumerate() {
            for (c, cell) in rows.iter().enumerate() {
                let rule = cell.rule;
                let color = rule.get_color();
                printer.with_color(ColorStyle::back(color), |printer| printer.print((c, r), " "))
            }

            for ant in &self.ants {
                let cell = &self.cells[ant.pos.1][ant.pos.0];
                ant.print(printer, cell);
            }
        }
    }
}