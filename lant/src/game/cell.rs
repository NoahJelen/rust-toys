use super::*;

#[derive(Clone)]
pub struct Cell {
    pub rule_idx: usize,
    pub rule: Rule
}

impl Cell {
    pub fn new(rule: Rule) -> Cell {
        Cell {
            rule_idx: 0,
            rule
        }
    }
}

#[derive(Copy, Clone)]
pub struct CellPos(pub usize, pub usize);

impl CellPos {
    pub fn offset(&mut self, direction: Direction, max_x: usize, max_y: usize) {
        match direction {
            Direction::East => {
                if self.0 >= max_x - 1 {
                    self.0 = 0;
                }
                else {
                    self.0 += 1;
                }
            },

            Direction::West => {
                if self.0 == 0 {
                    self.0 = max_x - 1;
                }
                else {
                    self.0 -= 1;
                }
            },

            Direction::North => {
                if self.1 == 0 {
                    self.1 = max_y - 1;
                }
                else {
                    self.1 -= 1;
                }
            },

            Direction::South => {
                if self.1 >= max_y - 1 {
                    self.1 = 0;
                }
                else {
                    self.1 += 1;
                }
            }
        }
    }
}