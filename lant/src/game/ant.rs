use super::*;
use cursive::{
    Printer,
    theme::{Color, ColorStyle}
};

pub struct Ant {
    pub pos: CellPos,
    direction: Direction,
    color: Color
}

impl Ant {
    pub fn new(x: usize, y: usize, color: Color) -> Ant {
        Ant {
            pos: CellPos(x, y),
            direction: Direction::East,
            color
        }   
    }

    pub fn advance(&mut self, max_x: usize, max_y: usize, rule: Rule) {
        self.direction = self.direction.apply_rule(rule);
        self.pos.offset(self.direction, max_x, max_y);
    }

    pub fn print(&self, printer: &Printer, cell: &Cell) {
        let rule = cell.rule;

        let style = ColorStyle::new(
            self.color,
            rule.get_color()
        );

        printer.with_color(style, |printer| {
            printer.print((self.pos.0, self.pos.1), "#");
        });
    }
}