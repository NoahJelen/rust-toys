use crate::game::*;
use rand::{Rng, rngs::ThreadRng};
use cursive::{
    theme::{Color, BaseColor},
    traits::View,
    Printer,
    Vec2
};

const ANT_COLORS: [Color; 8] = [Color::Light(BaseColor::Red), Color::Light(BaseColor::Black), Color::Light(BaseColor::Green), Color::Light(BaseColor::Yellow), Color::Light(BaseColor::Blue), Color::Light(BaseColor::Magenta), Color::Light(BaseColor::Cyan), Color::Light(BaseColor::White)];
const RULE_COLORS: [Color; 8] = [Color::TerminalDefault, Color::Dark(BaseColor::White), Color::Dark(BaseColor::Red), Color::Dark(BaseColor::Green), Color::Dark(BaseColor::Yellow), Color::Dark(BaseColor::Blue), Color::Dark(BaseColor::Magenta), Color::Dark(BaseColor::Cyan)];

pub struct LangtonAntView {
    width: usize,
    height: usize,
    board: Board,
    num_ants: usize,
    rand: ThreadRng
}

impl LangtonAntView {
    pub fn new(rule_str: &str, num_ants: usize) -> LangtonAntView{
        let mut rand = rand::thread_rng();
        let ruleset = gen_rules(rule_str, &mut rand);
        let ants: Vec<Ant> = Vec::with_capacity(num_ants);
        let board = Board::new(ants, Rule::Left(Color::TerminalDefault), 0, 0, ruleset);

        LangtonAntView {
            width: 0,
            height: 0,
            board,
            num_ants,
            rand
        }
    }

    pub fn update(&mut self) {
        if (self.width, self.height) == (0, 0) {
            return;
        }
        self.board.tick();
    }
}

impl View for LangtonAntView {
    fn draw(&self, printer: &Printer) {
        if (self.width, self.height) == (0, 0) {
            return;
        }
        self.board.print(printer);
    }

    fn required_size(&mut self, size: Vec2) -> Vec2 {
        if self.width > 0 && self.height > 0 {
            return size;
        }
        self.width = size.x;
        self.height = size.y;
        let rules = self.board.rules.clone();
        let ants = gen_ants(self.num_ants, self.width, self.height, &mut self.rand);
        let board = Board::new(ants, Rule::Left(Color::TerminalDefault), size.x, size.y, rules);
        self.board = board;
        size
    }
}

fn gen_rules(rule_str: &str, rand: &mut ThreadRng) -> Vec<Rule> {
    let mut rules: Vec<Rule> = Vec::new();
    for (i, letter) in rule_str.to_uppercase().chars().enumerate() {
        match letter {
            'R' => rules.push(Rule::Right(gen_color(i, rand, true))),
            'L' => rules.push(Rule::Left(gen_color(i, rand, true))),
            'S' => rules.push(Rule::Straight(gen_color(i, rand, true))),
            'U' => rules.push(Rule::Reverse(gen_color(i, rand, true))),
            _ => return vec![Rule::Right(Color::TerminalDefault), Rule::Left(Color::Dark(BaseColor::White))]
        }
    }
    rules
}

fn gen_ants(num_ants: usize, max_x: usize, max_y: usize, rand: &mut ThreadRng) -> Vec<Ant> {
    let mut ants: Vec<Ant> = Vec::new();
    for i in 0..num_ants {
        let color = gen_color(i, rand, false);
        ants.push(Ant::new(rand.gen_range(0..max_x), rand.gen_range(0..max_y), color));
    }
    ants
}

fn gen_color(num: usize, rand: &mut ThreadRng, is_rule_color: bool) -> Color {
    if num >= 8 {
        Color::Rgb(rand.gen(), rand.gen(), rand.gen())
    }
    else if is_rule_color{
        RULE_COLORS[num]
    }
    else {
        ANT_COLORS[num]
    }
}