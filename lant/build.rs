use std::fs;
use chrono::{Datelike, Timelike, Local};
use rust_utils::utils;

const MONTHS: [&str; 12] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    let date = Local::now().date();
    let month = MONTHS[date.month() as usize - 1];
    let day = date.day();
    let year = date.year();
    let date_str = format!("{day} {month} {year}");
    let date_version = utils::run_command("git", false, ["show", "-s", "--format=%cd", "--date=format:%Y.%m.%d", "-1"]).output;
    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second(); 
    let time_str = format!("{hour:02}:{minute:02}:{second:02}");
    let manpage = format!(
        ".\\\" Manpage for lant (part of The Rust Toys).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH lant 1 \"{date_str}\" \"{date_version}\" \"lant man page\"\n\
        .SH NAME\n\
        lant [options] \\- command-line Langton's Ant simulation. It is not entirely accurate.\n\
        .SH SYNOPSIS\n\
        lant -r <rule string>\n\
        .br\n\
        lant -a <number of ants>\n\n\
        .SH DESCRIPTION\n\
        My attempt at a Langton's Ant simulation\n\n\
        .SH OPTIONS\n\
        Use -r <rule string> to specify the rules used\n\
        .br\n\n\
        .br\n\
        Use -a to specify the number of ants to use\n\
        .br\n\
        Use -m to allow stepping through the simulation with the enter key.\n\
        .SH EXAMPLES\n\
        lant -a 20\n\
        .br\n\
        lant -r RRLL -a 2\n\n\
        .SH SEE ALSO\n\
        oximatrix(1)\n\
        .br\n\
        oxipipes(1)\n\
        .br\n\
        gol(1)\n\
        .br\n\
        2048(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );
    fs::create_dir("../man").unwrap_or(());
    fs::remove_file("../man/lant.1").unwrap_or(());
    fs::write("../man/lant.1", manpage).unwrap();
}