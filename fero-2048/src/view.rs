use cursive::{
    Printer,
    traits::View,
    theme::{BaseColor, Color, ColorStyle},
    Vec2
};
use rand::{Rng, thread_rng};
use crate::game;

pub struct Fero2048View {
    pub game: game::Game,
    width: usize,
    height: usize,
    pub is_paused: bool
}

impl Fero2048View {
    pub fn new(game: game::Game) -> Fero2048View{
        Fero2048View {
            game,
            width: 0,
            height: 0,
            is_paused: false
        }
    }
}

impl View for Fero2048View {
    fn draw(&self, printer: &Printer) {
        for r in 0..self.game.board_size {
            for c in 0..self.game.board_size {
                if self.game.board.field[r][c] != 0 {
                    draw_tile((6 * c, 3 * r), self.game.board.field[r][c], printer);
                }
            }
        }
    }

    fn required_size(&mut self, _constraint: Vec2) -> Vec2 {
        let size = self.game.board_size;
        self.width = size * 6;
        self.height = size * 3;
        Vec2::new(self.width, self.height)
    }
}

fn draw_tile(coord: (usize,usize), val: u32, printer: &Printer) {
    let mut random =  thread_rng();
    let (x,y) = coord;
    let mut text_color = Color::Light(BaseColor::White);
    let color = match val {
        1    => Color::Light(BaseColor::White),
        2    => Color::Light(BaseColor::Yellow),
        4    => Color::Light(BaseColor::Red),
        8    => Color::from_256colors(208),
        16   => Color::Dark(BaseColor::Blue),
        32   => Color::Dark(BaseColor::Green),
        64   => Color::from_256colors(135),
        128  => Color::from_256colors(88),
        256  => Color::from_256colors(39),
        512  => Color::from_256colors(20),
        1024 => Color::from_256colors(45),
        _ => Color::Rgb(random.gen(),random.gen(),random.gen())
    };

    if color == Color::Light(BaseColor::White) || color == Color::Light(BaseColor::Yellow) {
        text_color = Color::Dark(BaseColor::Black);
    }

    let style = ColorStyle::new(
        text_color,
        color
    );

    for w in x..x + 6 {
        for h in y..y + 3 {
            printer.with_color(style, |printer| {
                printer.print((w, h), " ");
            });
        }
    }
    printer.with_color(style, |printer| {
        if (1024..=65536).contains(&val) {
            printer.print((x + 1, y + 1), val.to_string().as_str());
        }
        else {
            printer.print((x + 2, y + 1), val.to_string().as_str());
        }
    }); 
}   