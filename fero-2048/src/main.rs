use cursive::{
    event::{self, Key},
    theme::{BaseColor, BorderStyle, Color, PaletteColor, Theme},
    views::{Dialog, OnEventView, ResizedView, SelectView, TextView, ViewRef},
    view::{Nameable, SizeConstraint},
    With
};
use cursive_extras::*;

mod game;
mod view;
use view::*;

// Fero 2048 is basically a dumbed down version of 2048 Vanced for the command line
// it has most of 2048 Vanced's functionality but uses ncurses (cursive) instead of OpenGL (piston)
// This may become one of the games on a default AOS install
fn main() {
    let mut theme_def = Theme {
        shadow: false,
        ..Theme::default()
    };
    theme_def.palette[PaletteColor::Background] = Color::TerminalDefault;
    theme_def.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
    theme_def.palette[PaletteColor::View] = Color::TerminalDefault;
    theme_def.palette[PaletteColor::Highlight] = Color::Light(BaseColor::Blue);
    theme_def.palette[PaletteColor::HighlightText] = Color::Dark(BaseColor::Black);
    theme_def.palette[PaletteColor::Secondary] = Color::Dark(BaseColor::Blue);
    theme_def.palette[PaletteColor::TitlePrimary] = Color::Light(BaseColor::Blue);
    theme_def.borders = BorderStyle::Outset;
    let theme_def2 = theme_def.clone();
    let mut size_select = cursive::default();
    size_select.set_theme(theme_def);
    let mut boards: SelectView<usize> = SelectView::new();

    for i in 4..=7 {
        boards.add_item(i.to_string(), i);
    }

    size_select.add_layer(
        Dialog::around(
            boards
                .on_submit(|view, _| view.quit())
                .with_name("boards")
        )
            .title("Select Board Size")
    );

    size_select.set_on_pre_event(cursive::event::Event::CtrlChar('c'), |_| panic!());
    size_select.run();
    let s: ViewRef<SelectView<usize>> = size_select.find_name("boards").unwrap();
    let size = *s.selection().unwrap();
    let game = game::Game::load(size);
    let mut game_view = cursive::default();
    game_view.set_theme(theme_def2);
    game_view.add_layer(
        ResizedView::with_fixed_height(size * 3 + 2,
            Dialog::around(
                hlayout!(
                    TextView::new("Score: 0\nBest: 0\n\nQuit: q\nPause: p\nReset Game: r").with_name("scores"),
                    HDivider::new(),
                    Fero2048View::new(game).with_name("game")
                )
            )
                .title("2048")
        )
            .with_name("outer")
    );

    game_view.set_global_callback(event::Event::Refresh, |view| {
        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
        let size = game.game.board_size * 3 + 2;
        game.game.update_best();
        let score = game.game.score;
        let best = game.game.best;
        let game_over = game.game.is_over();
        let won = game.game.won();
        let score_str = format!("Score: {score}\nBest: {best}\n\nQuit: q\nPause: p\nReset Game: r");
        view.call_on_name("scores", |scores: &mut TextView| scores.set_content(score_str));
        view.call_on_name("outer", |outer: &mut ResizedView<Dialog>| outer.set_height(SizeConstraint::Fixed(size)));
        if won {
            view.update_theme(|theme| {
                theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::Green);
                theme.palette[PaletteColor::Tertiary] = Color::Light(BaseColor::Green);
            });
        }

        if game_over {
            view.set_fps(0);
            view.update_theme(|theme| {
                theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::Red);
                theme.palette[PaletteColor::Tertiary] = Color::Light(BaseColor::Red);
            });
            view.add_layer(
                Dialog::text("Game over!")
                    .title("Game over!")
                    .button("Quit", |view| view.quit())
                    .button("Reset", |view|{
                        view.set_fps(30);
                        view.call_on_name("game",|game: &mut Fero2048View| game.game.reset());
                        view.update_theme(|theme|{
                        theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
                            theme.palette[PaletteColor::Tertiary] = Color::Light(BaseColor::White);
                        });
                        view.pop_layer();
                    })
            );
        }
    });

    game_view.set_global_callback(event::Key::Down, |view|{
        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
        if game.is_paused {
            return;
        }
        check_move(&mut game.game, event::Key::Down);
    });

    game_view.set_global_callback(event::Key::Up, |view| {
        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
        if game.is_paused {
            return;
        }
        check_move(&mut game.game, event::Key::Up);
    });

    game_view.set_global_callback(event::Key::Left, |view| {
        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
        if game.is_paused {
            return;
        }
        check_move(&mut game.game, event::Key::Left);
    });

    game_view.set_global_callback(event::Key::Right, |view| {
        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
        if game.is_paused {
            return;
        }
        check_move(&mut game.game, event::Key::Right);
    });

    game_view.set_global_callback('r', |view|{
        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
        view.update_theme(|theme| {
            theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
            theme.palette[PaletteColor::Tertiary] = Color::Light(BaseColor::White);
        });
        game.game.reset();
    });

    game_view.set_global_callback('p', |view| {
        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
        game.is_paused = true;
        let pause = vec!["Resume", "Change Board Size", "Quit"];
        let mut pause_menu: SelectView<usize> = SelectView::new();
        for (i, item) in pause.iter().enumerate() {
            pause_menu.add_item(*item, i);
        }
        view.add_layer(
            Dialog::around(pause_menu
                .on_submit(|view, val|{
                    if *val == 0 {
                        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
                        game.is_paused =  false;
                        view.pop_layer();
                    }
                    if *val == 1 {
                        let mut boards: SelectView<usize> = SelectView::new();
                        for i in 4..=7 {
                            boards.add_item(i.to_string(), i);
                        }
                        view.add_layer(
                            Dialog::around(
                                boards
                                    .on_submit(|view, val| {
                                        view.update_theme(|theme| {
                                            theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
                                            theme.palette[PaletteColor::Tertiary] = Color::Light(BaseColor::White);
                                        });
                                        let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
                                        game.is_paused =  false;
                                        let new_game = game::Game::load(*val);
                                        game.game = new_game;
                                        view.pop_layer();
                                        view.pop_layer();
                                    })
                                    .wrap_with(OnEventView::new)
                                    .on_event(event::Event::Key(Key::Esc), |view| {
                                        view.pop_layer();
                                    })
                            )
                                .dismiss_button("Back")
                                .title("Select Board Size")
                    )
                }
                if *val == 2 {
                    view.pop_layer();
                    view.quit();
                }
            }))
                .title("Pause")
                .wrap_with(OnEventView::new)
                .on_event(event::Event::Key(Key::Esc), |view| {
                    let mut game: ViewRef<Fero2048View> = view.find_name("game").unwrap();
                    game.is_paused = false;
                    view.pop_layer();
                })
        )
    });

    game_view.set_global_callback('q', |view| view.quit());
    game_view.set_fps(30);
    game_view.run();
}

fn check_move(game: &mut game::Game, umove: event::Key) {
    let test = game.board.field.clone();
    let mut score = game.score;
    score += game.board.do_move(&umove);
    game.score = score;

    // generate new tiles if the move was valid
    if test != game.board.field {
        game.board.new_tile();
    }
    game.save();
}