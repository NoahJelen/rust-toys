use std::{env, fs, path::PathBuf};
use cursive::event::Key;

pub struct Game {
    pub score: u32,
    pub best: u32,
    pub board_size: usize,
    pub board: Board
}

impl Game {
    pub fn reset(&mut self) {
        self.board = Board::new(self.board_size);
        self.score = 0;
    }

    pub fn is_over(&self) -> bool {
        let board = &self.board;
        let mut test_board = board.clone();
        let moves = [Key::Up, Key::Down, Key::Left, Key::Right];
        for i in moves {
            test_board.do_move(&i);
            let test = test_board.field.clone();
            let field = board.field.clone();
            if test != field {
                return false;
            }

            if i == Key::Right {
                return true;
            }
        }
        true
    }

    pub fn won(&self) -> bool {
        let field = self.board.field.clone();

        for row in field.iter(){
            if row.iter().any(|x| *x == 2048) {
                return true;
            }
        }
        false
    }

    pub fn save(&self) {
        let game_dir = PathBuf::from(format!("{}/.local/share/fero-2048/", env::var("HOME").expect("Where the hell is your home folder?!")));
        let save_file = game_dir.join(format!("save.{}", self.board_size));
        fs::create_dir_all(&game_dir).unwrap_or(());
        let mut save_str = String::new();
        let score_str = format!("{}\n", self.score);
        save_str.push_str(&score_str);
        let best_str = format!("{}\n", self.best);
        save_str.push_str(&best_str);
        let mut num_row = "".to_string();
        let mut num_str: String;
        for array in &self.board.field {
            for num in array {
                num_str = num.to_string();
                num_row.push_str(&num_str);
                num_row.push(' ');
            }
            num_row.pop();
            num_row.push('\n');
        }
        save_str.push_str(&num_row);
        save_str.pop();
        fs::write(&save_file, save_str).expect("Unable to save game!");
    }

    pub fn update_best(&mut self) {
        if self.score > self.best {
            self.best = self.score;
        }
    }

    pub fn load(board_size: usize) -> Game {
        let game_dir = PathBuf::from(format!("{}/.local/share/fero-2048/",env::var("HOME").expect("Where the hell is your home folder?!")));
        let load_file = game_dir.join(format!("save.{}",&board_size));
        let mut field = vec![vec![0;board_size];board_size];
        let mut score = 0;
        let mut best = 0;
        let data = fs::read_to_string(load_file).unwrap_or_default();
        if !data.is_empty() {
            let lines: Vec<&str> = data.lines().collect();
            score = lines[0].to_string().parse().unwrap();
            best = lines[1].to_string().parse().unwrap();

            for (r, row) in field.iter_mut().enumerate() {
                let nline: Vec<&str> = lines[r + 2].split(' ').collect();
                for (c, cell) in row.iter_mut().enumerate() {
                    *cell = nline[c].parse::<u32>().unwrap();
                }
            }
        }
        else {
            field = Board::new(board_size).field;
        }
        Game {
            board: Board{ field },
            score,
            best,
            board_size
        }
    }
}

#[derive(Clone)]
pub struct Board {
    pub field: Vec<Vec<u32>>
}

impl Board {
    pub fn new(size: usize) -> Board {
        let field = vec![vec![0; size]; size];
        let mut board = Board{field};

        //add 2 new tiles to the new board
        board.new_tile();
        board.new_tile();
        board
    }

    pub fn new_tile(&mut self){
        let size = self.field.len();
        let mut field = self.field.clone();
        for _ in 0..size ^ 2 {
            let x: usize = rand::random();
            if field[x % size][(x / size) % size] == 0 {
                if x % 5 == 0 {
                    field[x % size][(x / size) % size] = 4;
                }
                else if x % 10 == 0 {
                    field[x % size][(x / size) % size] = 2;
                }
                else {
                    field[x % size][(x / size) % size] = 1;
                }
                break;
            }
        }
        self.field = field;
    }

    pub fn do_move(&mut self, usermove: &Key) -> u32{
        let size = self.field.len();
        let field = &mut self.field;
        let mut score = 0;
        match *usermove {
            Key::Left => {
                for array in field{
                    for col in 0..size {
                        for testcol in (col + 1)..size {
                            if array[testcol] != 0 {
                                if array[col] == 0 {
                                    array[col] += array[testcol];
                                    array[testcol] = 0;
                                }
                                else if array[col] == array[testcol] {
                                    array[col] += array[testcol];
                                    score += array[testcol];
                                    array[testcol] = 0;
                                    break;
                                }
                                else {
                                    break
                                }
                            }
                        }
                    }
                }
                score * 2
            },
            Key::Right => {
                for array in field {
                    for  col in (0..size).rev() {
                        for testcol in (0..col).rev() {
                            if array[testcol] != 0 {
                                if array[col] == 0 {
                                    array[col] += array[testcol];
                                    array[testcol] = 0;
                                }
                                else if array[col] == array[testcol] {
                                    array[col] += array[testcol];
                                    score += array[testcol];
                                    array[testcol] = 0;
                                    break;
                                }
                                else {
                                    break;
                                }
                            }
                        }
                    }
                }
                score*2
            },
            Key::Down => {
                for col in 0..size {
                    for row in (0..size).rev() {
                        for testrow in (0..row).rev() {
                            if field[testrow][col] != 0 {
                                if field[row][col] == 0 {
                                    field[row][col] += field[testrow][col];
                                    field[testrow][col] = 0;
                                }
                                else if field[row][col] == field[testrow][col] {
                                    field[row][col] += field[testrow][col];
                                    score += field[testrow][col];
                                    field[testrow][col] = 0;
                                    break;
                                }
                                else {
                                    break;
                                }
                            }
                        }
                    }
                }
                score * 2
            },
            Key::Up => {
                for col in 0..size {
                    for row in 0..size {
                        for testrow in (row+1)..size {
                            if field[testrow][col] != 0 {
                                if field[row][col] == 0 {
                                    field[row][col] += field[testrow][col];
                                    field[testrow][col] = 0;
                                }
                                else if field[row][col] == field[testrow][col] {
                                    field[row][col] += field[testrow][col];
                                    score += field[testrow][col];
                                    field[testrow][col] = 0;
                                    break;
                                }
                                else {
                                    break;
                                }
                            }
                        }
                    }
                }
                score * 2
            },
            _=> 0
        }
    }
}