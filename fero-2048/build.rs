use std::fs;
use chrono::{Datelike, Timelike, Local};
use rust_utils::utils;

const MONTHS: [&str; 12] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    let date = Local::now().date();
    let month = MONTHS[date.month() as usize - 1];
    let day = date.day();
    let year = date.year();
    let date_str = format!("{day} {month} {year}");
    let date_version = utils::run_command("git", false, ["show", "-s", "--format=%cd", "--date=format:%Y.%m.%d", "-1"]).output;
    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second(); 
    let time_str = format!("{hour:02}:{minute:02}:{second:02}");
    let manpage = format!(
        ".\\\" Manpage for 2048 (part of The Rust Toys).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH 2048 1 \"{date_str}\" \"{date_version}\" \"2048 man page\"\n\
        .SH NAME\n\
        2048 \\- command line 2048\n\n\
        .SH DESCRIPTION\n\
        Yet another 2048 game in Rust!\n\n\
        .SH SEE ALSO\n\
        oximatrix(1)\n\
        .br\n\
        oxipipes(1)\n\
        .br\n\
        gol(1)\n\
        .br\n\
        lant(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com) "
    );
    fs::create_dir("../man").unwrap_or(());
    fs::remove_file("../man/2048.1").unwrap_or(());
    fs::write("../man/2048.1", manpage).unwrap();
}