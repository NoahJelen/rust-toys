use std::fs;
use chrono::{Datelike, Timelike, Local};
use rust_utils::utils;

const MONTHS: [&str; 12] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    let date = Local::now().date();
    let month = MONTHS[date.month() as usize - 1];
    let day = date.day();
    let year = date.year();
    let date_str = format!("{day} {month} {year}");
    let date_version = utils::run_command("git", false, ["show", "-s", "--format=%cd", "--date=format:%Y.%m.%d", "-1"]).output;
    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second(); 
    let time_str = format!("{hour:02}:{minute:02}:{second:02}");
    let manpage = format!(
        ".\\\" Manpage for gol (part of The Rust Toys).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH gol 1 \"{date_str}\" \"{date_version}\" \"gol man page\"\n\
        .SH NAME\n\
        gol [options] \\- game of life simulation\n\
        .SH SYNOPSIS\n\
        gol -c <color>\n\
        .br\n\
        gol -s <seed name>\n\n\
        .SH DESCRIPTION\n\
        My attempt at a game of life simulation.\n\n\
        .SH OPTIONS\n\
        Use -c to specify the colors of the cells. Multiple colors can be used, separated by commas.\n\
        .br\n\
        Valid colors are:\n\
        .br\n    \
        gray, gs, or grayscale: Random grayscale colors\n\
        .br\n    \
        multi: Random colors\n\
        .br\n    \
        multib: Random basic colors\n\
        .br\n    \
        black\n\
        .br\n    \
        blue\n\
        .br\n    \
        cyan\n\
        .br\n    \
        green\n\
        .br\n    \
        magenta\n\
        .br\n    \
        red\n\
        .br\n    \
        white\n\
        .br\n    \
        yellow\n\
        .br\n    \
        Any number from 0-255 representing a 8-bit color\n\
        .br\n    \
        Any hex number representing a 24-bit color\n\
        .br\n\n\
        .br\n\
        Use -s <name> to specify a seed.\n\
        .br\n\
        Valid names are:\n\
        .br\n    \
        agar\n\
        .br\n    \
        glider-gun\n\
        .br\n    \
        glider\n\
        .br\n    \
        pseudo-period-gun\n\
        .br\n    \
        pufferfish-spaceship\n\
        .br\n    \
        reaction\n\
        .br\n    \
        spaceship\n\
        .br\n    \
        random\n\n\
        .SH EXAMPLES\n\
        gol -c red,white,blue\n\
        .br\n\
        gol -c gs\n\
        .br\n\
        gol -c red, green, blue -s agar\n\
        .br\n\
        gol -s glider\n\n\
        .SH SEE ALSO\n\
        oximatrix(1)\n\
        .br\n\
        oxipipes(1)\n\
        .br\n\
        2048(1)\n\
        .br\n\
        lant(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );
    fs::create_dir("../man").unwrap_or(());
    fs::remove_file("../man/gol.1").unwrap_or(());
    fs::write("../man/gol.1", manpage).unwrap();
}