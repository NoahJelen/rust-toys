use rand::Rng;
use crate::{seeds, view};
use cursive::theme::{BaseColor, Color};

#[derive(Clone, Copy, Debug)]
pub struct Cell {
    pub state: CellState,
    pub color: Color
}

impl Cell {
    pub fn new(state: CellState, color: Color) -> Cell {
        Cell {
            state,
            color
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum CellState {
    Alive,
    Dead
}

#[derive(Clone, Debug)]
pub struct Board {
    pub width: usize,
    pub height: usize,
    pub cells: Vec<Vec<Cell>>,
    pub colors: Vec<String>
}

impl Board {
    pub fn new(width: usize, height: usize, colors: &[String], seed: &str) -> Board {
        let board_colors = colors.to_vec();
        let cells = seeds::get_seed(width, height, seed, colors);

        Board {
            width,
            height,
            cells,
            colors: board_colors
        }
    }

    fn get_cell(&self, x: usize, y: usize) -> Cell { self.cells[y][x] }

    fn get_live_neighbors(&self, x: usize, y: usize) -> usize {
        let max_x = self.width - 1;
        let max_y = self.height - 1;
        let mut live_neighbors = 0;
        for lx in -1..=1 {
            for ly in -1..=1 {
                let new_x: usize = match x as isize + lx {
                    -1 => {
                        max_x
                    },
                    _ => {
                        if x as isize + lx > (max_x as isize) {
                            0
                        }
                        else {
                            (x as isize + lx) as usize
                        }
                    }
                };

                let new_y: usize = match y as isize + ly {
                    -1 => {
                        max_y
                    },
                    _ => {
                        if y as isize + ly > (max_y as isize) {
                            0
                        }
                        else {
                            (y as isize + ly) as usize
                        }
                    }
                };

                let neighbor = self.get_cell(new_x, new_y);
                if lx == 0 && ly == 0 {
                    continue;
                }

                if neighbor.state == CellState::Alive {
                    live_neighbors += 1;
                }
            }
        }

        live_neighbors
    }
    
    pub fn evolve(&mut self) {
        let mut rand = rand::thread_rng();
        let mut future = vec![vec![Cell::new(CellState::Dead, Color::Dark(BaseColor::Black));self.width];self.height];

        for row in &mut future {
            for mut cell in row {
                let cell_colors: Vec<Color> = self.colors.iter().map(|color| view::get_color(color, &mut rand)).collect();
                let color = cell_colors[rand.gen_range(0..cell_colors.len())];
                cell.color = color;
            }
        }

        for (r, row) in future.iter_mut().enumerate() {
            for (c, cell) in row.iter_mut().enumerate() {
                let num_neighbors = self.get_live_neighbors(c, r);
                if self.get_cell(c, r).state == CellState::Alive && !(2..=3).contains(&num_neighbors) {
                    cell.state = CellState::Dead;
                }
                else if self.get_cell(c, r).state == CellState::Dead && num_neighbors == 3 {
                    cell.state = CellState::Alive;
                }
                else {
                    *cell = self.get_cell(c, r);
                }
            }
        }

        self.cells = future;
    }
}