use crate::{
    view,
    game::{CellState, Cell}
};
use rand::{Rng, thread_rng};
use cursive::theme::{Color, BaseColor};
use rand::seq::SliceRandom;

pub fn is_valid_name(name: &str) -> bool {
    matches!(name, "agar" | "glider-gun" | "glider" | "pseudo-period-gun" | "pufferfish-spaceship" | "reaction" | "spaceship" | "random")
}

pub fn get_seed(x_size: usize, y_size: usize, name: &str, colors: &[String]) -> Vec<Vec<Cell>> {
    let mut rand = thread_rng();
    let mut field = vec![vec![Cell::new(CellState::Dead, Color::Dark(BaseColor::Black)); x_size]; y_size];
    if x_size == 0 && y_size == 0 {
        return field;
    }

    if name == "random" {
        for row in &mut field {
            for mut cell in row {
                let cell_colors: Vec<Color> = colors.iter().map(|color| view::get_color(color, &mut rand)).collect();
                let color = cell_colors.choose(&mut rand).unwrap();
                cell.color = *color;
                cell.state = match rand.gen_range(0..5) {
                    3 => CellState::Alive,
                    _ => CellState::Dead
                };
            }
        }
        return field;
    }
    
    let seed = match name {
        "agar" => include_str!("agar.txt"),
        "glider-gun" => include_str!("glider-gun.txt"),
        "glider" => include_str!("glider.txt"),
        "pseudo-period-gun" => include_str!("pseudo-period-gun.txt"),
        "pufferfish-spaceship" => include_str!("pufferfish-spaceship.txt"),
        "reaction" => include_str!("reaction.txt"),
        "spaceship" => include_str!("spaceship.txt"),
        _ => unreachable!()
    };

    for row in &mut field {
        for mut cell in row {
            let cell_colors: Vec<Color> = colors.iter().map(|color| view::get_color(color, &mut rand)).collect();
            let color = cell_colors.choose(&mut rand).unwrap();
            cell.color = *color;
        }
    }

    for (r, row) in seed.split('\n').enumerate() {
        for (c, ch) in row.chars().enumerate() {
            if r >= y_size || c >= x_size {
                continue;
            }
            match ch {
                '*' => field[r][c].state = CellState::Alive,
                ' ' => field[r][c].state = CellState::Dead,
                _ => panic!("Invalid char!")
            }
        }
    }

    field
}