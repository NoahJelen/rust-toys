use cursive::{
    event,
    theme::{Theme, PaletteColor, Color, BaseColor},
    views::ResizedView,view::Nameable
};
use std::env;
use crate::view::GOLView;

mod game;
mod view;
mod seeds;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut colors: Vec<String> = vec!["MULTI".to_string()];
    let mut seed = "random";
    for (i, arg) in args.iter().enumerate() {
        match arg.as_str() {
            "--color" | "-c" => {
                if i >= (args.len() - 1) {
                    println!("Please specify a color!");
                    return;
                }
    
                let c = &args[i + 1];
                let color_vec: Vec<String> = c.split(',').map(|color| color.to_string()).collect();
    
                for color in color_vec {
                    if view::get_color(&color.to_string(), &mut rand::thread_rng()) == Color::TerminalDefault {
                        println!("Invalid color: {color}");
                        println!("Examples of valid colors:");
                        println!("The 8 TTY colors: black, blue, cyan, green, magenta, red, white, and yellow");
                        println!("0-255 representing any of the 256 colors");
                        println!("or a 6 digit hex code representing a color");
                        return;
                    }
                }
    
                colors = args[i + 1]
                    .split(',')
                    .map(|color_str| color_str.to_string())
                    .collect();
            },

            "--seed" | "-s" => {
                if seeds::is_valid_name(&args[i + 1]) {
                    seed = &args[i + 1];
                }
                else {
                    eprintln!("Invalid seed name!");
                    return;
                }
            },

            _ => { }
        }
    }

    let mut theme = Theme::default();
    theme.palette[PaletteColor::Background] = Color::TerminalDefault;
    theme.palette[PaletteColor::View] = Color::TerminalDefault;
    theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
    let mut root = cursive::default();
    root.set_theme(theme);
    root.add_fullscreen_layer(ResizedView::with_full_screen(GOLView::new(colors, seed).with_name("gol")));
    
    root.add_global_callback(event::Event::Refresh, |view| {
        let mut gol = view.find_name::<GOLView>("gol").unwrap();
        gol.update();
    });

    root.add_global_callback('q', |view| view.quit());
    root.set_fps(30);
    root.run();
}