use cursive::{
    theme::{ColorStyle, Color, BaseColor},
    View,
    Printer,
    Vec2
};
use rand::{Rng, rngs::ThreadRng};
use crate::game::{Board, CellState};

pub struct GOLView {
    width: usize,
    height: usize,
    board: Board,
    colors: Vec<String>,
    seed: String
}

impl GOLView {
    pub fn new(colors: Vec<String>, seed: &str) -> GOLView {
        let board = Board::new(0, 0, &colors, seed);

        GOLView {
            width: 0,
            height: 0,
            board,
            colors,
            seed: seed.to_string()
        }
    }

    pub fn update(&mut self) {
        if self.board.width == 0 || self.board.height == 0 {
            self.board = Board::new(self.width, self.height,&self.colors, &self.seed);
            return;
        }

        if self.board.width != self.width || self.board.height != self.height {
            self.board = Board::new(self.width, self.height, &self.colors, &self.seed);
            return;
        }

        self.board.evolve();
    }
}

impl View for GOLView {
    fn draw(&self, printer: &Printer) {
        for r in 0..self.board.height {
            for c in 0..self.board.width {
                if c == self.board.cells[0].len() {
                    break;
                }
                let style = ColorStyle::new(Color::TerminalDefault, self.board.cells[r][c].color);
                match self.board.cells[r][c].state {
                    CellState::Alive => printer.with_color(style, |printer| printer.print((c, r), " ")),
                    CellState::Dead => printer.print((c, r), " ")
                }
            }
        }
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x;
        self.height = constraint.y;
        Vec2::new(self.width, self.height)
    }
}

pub fn get_color(color_str: &str, rand: &mut ThreadRng) -> Color {
    let basic_colors = [BaseColor::Black,BaseColor::Blue,BaseColor::Cyan,BaseColor::Green,BaseColor::Magenta,BaseColor::Red,BaseColor::White,BaseColor::Yellow];
    let mut basecolor =  BaseColor::Black;
    let mut is_base = true;
    let is_dark: bool = rand.gen();

    // is the color a named color?
    match color_str.to_uppercase().as_str() {
        "GRAY" | "GS" | "GRAYSCALE" => {
            let a: u8 = rand.gen();
            return Color::Rgb(a, a, a);
        },
        "MULTI" => return Color::Rgb(rand.gen(),rand.gen(),rand.gen()),
        "MULTIB" => basecolor = basic_colors[rand.gen_range(0..8)],
        "BLACK" => basecolor = BaseColor::Black,
        "BLUE" => basecolor = BaseColor::Blue,
        "CYAN" => basecolor = BaseColor::Cyan,
        "GREEN" => basecolor = BaseColor::Green,
        "MAGENTA" => basecolor = BaseColor::Magenta,
        "RED" => basecolor = BaseColor::Red,
        "WHITE" => basecolor = BaseColor::White,
        "YELLOW" => basecolor = BaseColor::Yellow,
        _ => is_base = false
    }

    if is_base {
        return if is_dark {
            Color::Dark(basecolor)
        }
        else {
            Color::Light(basecolor)
        }
    }

    // is it a number (0-255)?
    if let Ok(num) = color_str.parse::<u8>() {
        return Color::from_256colors(num)
    }

    // is it a hex string?
    if let Ok(cols) = hex::decode(color_str) {
        return Color::Rgb(cols[0], cols[1], cols[2])
    }
    Color::TerminalDefault
}