use std::{
    fs, env, thread,
    collections::HashSet,
    path::PathBuf,
    time::{Instant, Duration},
    process::{Child, Command, Stdio}
};
use rand::{
    thread_rng,
    prelude::ThreadRng,
    seq::SliceRandom
};
use lazy_static::lazy_static;
use regex::Regex;
use image::io::Reader as ImageReader;
use colored::Colorize;

lazy_static! {
    static ref ALBUM_RE: Regex = Regex::new(r"[Ff]older\.[a-zA-Z]*").expect("Invalid Regex!");
}

pub struct GlavaProcess {
    process: Option<Child>,
    colors: Vec<(u8, u8, u8)>,
    rand: ThreadRng,
    cur_song: String
}

impl GlavaProcess {
    pub fn new() -> GlavaProcess {
        let rand = thread_rng();
        let process = Some(Command::new("glava")
            .args(["--desktop","-a", "fifo"])
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .spawn()
            .unwrap());
        
        let colors = get_album_colors();
        GlavaProcess {
            process,
            colors,
            rand,
            cur_song: get_cur_song()
        }
    }

    pub fn update(&mut self, time: Instant) {
        if time.elapsed().as_secs() % 30 == 0 {
            self.change_colors();
            thread::sleep(Duration::from_secs(1));
        }
        else if song_changed(&self.cur_song) {
            self.cur_song = get_cur_song();
            let new_colors = get_album_colors();
            self.colors = new_colors;
            self.change_colors();
        }
    }

    fn change_colors(&mut self) {
        println!("Changing colors...");
        let mut process = self.process.take().unwrap();
        process.kill().unwrap_or(());
        process.wait().unwrap();
        println!("New colors:");
        let colors_in = format!(
            "#define C_LINE 1\n\
            #define BAR_WIDTH 5\n\
            #define BAR_GAP 1\n\
            #define BAR_OUTLINE_WIDTH 1\n\
            #define AMPLIFY 300\n\
            #define USE_ALPHA 0\n\
            #define GRADIENT 100\n\
            #define COLOR mix(vec4(mix(vec4(mix({}, {}, clamp(d / GRADIENT, 0, 1))), {}, clamp(d / GRADIENT, 0, 1))), vec4(mix({}, {}, clamp(d / GRADIENT, 0, 1))) ,clamp(d / GRADIENT, 0, 1))\n\
            #define BAR_OUTLINE vec4(COLOR.rgb * 1.5, COLOR.a)\n\
            #define DIRECTION 0\n\
            #define INVERT 0\n\
            #define FLIP 0\n\
            #define MIRROR_YX 0\n\
            #define DISABLE_MONO 0",
            self.get_color(),
            self.get_color(),
            self.get_color(),
            self.get_color(),
            self.get_color()
        );
        println!();
    
        fs::write("/tmp/glava-bars.glsl", colors_in).unwrap();
        self.process = Some(
            Command::new("glava")
                .args(["--desktop","-a", "fifo"])
                .stdout(Stdio::null())
                .stderr(Stdio::null())
                .spawn()
                .unwrap()
            );
    }

    fn get_color(&mut self) -> String {
        let c_t = self.colors.choose(&mut self.rand).unwrap();
        println!("#{:02X}{:02X}{:02X} {}", c_t.0, c_t.1, c_t.2, "   ".on_truecolor(c_t.0, c_t.1, c_t.2));
        format!("#{:02X}{:02X}{:02X}", c_t.0, c_t.1, c_t.2)
    }
}

fn get_cur_song() -> String {
    let mut cmd = Command::new("mpc");
    cmd.arg("current");
    let out = cmd.output().unwrap();
    let success = out.status.success();
    let mut o_fmt = if success { String::from_utf8(out.stdout).unwrap() } else { String::from_utf8(out.stderr).unwrap() };
    o_fmt.pop();
    o_fmt
}

fn song_changed(song: &str) -> bool {
    song != get_cur_song()
}

fn get_album_colors() -> Vec<(u8, u8, u8)> {
    let mut cmd = Command::new("mpc");
    cmd.args(["--format", "%file%", "current"]);
    let out = cmd.output().unwrap();
    let success = out.status.success();
    let mut o_fmt = if success { String::from_utf8(out.stdout).unwrap() } else { String::from_utf8(out.stderr).unwrap() };
    o_fmt.pop();
    let mut album_path = PathBuf::from(format!("{}/Music/{o_fmt}", env::var("HOME").expect("Where the hell is your home folder?!")));
    album_path.pop();
    for file in fs::read_dir(album_path).expect("unable to read album directory!") {
        let name_t = file.unwrap().path().into_os_string();
        let name = name_t.to_str().unwrap();
        if ALBUM_RE.is_match(name) {
            let img = ImageReader::open(&name).unwrap().with_guessed_format().unwrap().decode().unwrap();
            let img_bytes = img.as_bytes().to_vec();
            let palette = color_thief::get_palette(&img_bytes, color_thief::ColorFormat::Rgb, 1, 50).unwrap();
            let mut colors: Vec<(u8, u8, u8)> = Vec::new();
            println!("Album art colors:");
            for color in palette {
                colors.push(
                    (color.r, color.g, color.b)
                )
            }
            let mut uniques = HashSet::new();
            colors.retain(|c| uniques.insert(*c));
            for color in &colors {
                println!("#{:02X}{:02X}{:02X} {}", color.0, color.1, color.2, "   ".on_truecolor(color.0, color.1, color.2));
            }
            println!();
            return colors;
        }
    }
    vec![(90, 174, 222)]
}