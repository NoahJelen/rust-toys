#!/system/bin/sh
# Build script for Termux
rm $PREFIX/bin/{2048,oximatrix,oxipipes,gol,lant}
rm $PREFIX/man/man1/{2048.1,oximatrix.1,oxipipes.1,gol.1,lant.1}
cargo clean
cargo build --release
cp target/release/oximatrix $PREFIX/bin/oximatrix
cp target/release/2048 $PREFIX/bin/2048
cp target/release/oxipipes $PREFIX/bin/oxipipes
cp target/release/gol $PREFIX/bin/gol
cp target/release/lant $PREFIX/bin/lant
cp -r man/* $PREFIX/share/man/man1/
echo "Installed Rust Toys."