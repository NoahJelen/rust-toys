use std::fs;
use chrono::{Datelike, Timelike, Local};
use rust_utils::utils;

const MONTHS: [&str; 12] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    let date = Local::now().date();
    let month = MONTHS[date.month() as usize - 1];
    let day = date.day();
    let year = date.year();

    let date_str = format!("{day} {month} {year}");
    let date_version = utils::run_command("git", false, ["show", "-s", "--format=%cd", "--date=format:%Y.%m.%d", "-1"]).output;
    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second(); 
    let time_str = format!("{hour:02}:{minute:02}:{second:02}");
    let manpage = format!(
        ".\\\" Manpage for oximatrix (part of The Rust Toys).
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH oximatrix 1 \"{date_str}\" \"{date_version}\" \"oximatrix man page\"\n\
        .SH NAME\n\
        oximatrix [options] \\- command-line matrix rain simulation\n\
        .SH SYNOPSIS\n\
        oximatrix -c <color>\n\
        .br\n\
        oximatrix -r <max number of raindrops>\n\n\
        .SH DESCRIPTION\n\
        Command-line matrix rain screensaver\n\n\
        .SH OPTIONS\n\
        Use -r <number> to limit the amount of raindrops to a certain number\n\
        .br\n\n\
        .br\n\
        Use -c to specify the colors of the raindrops. Multiple colors can be used, separated by commas.\n\
        .br\n\
        Valid colors are:\n\
        .br\n    \
        gray, gs, or grayscale: Random grayscale colors\n\
        .br\n    \
        multi: Random colors\n\
        .br\n    \
        multib: Random basic colors\n\
        .br\n    \
        black\n\
        .br\n    \
        blue\n\
        .br\n    \
        cyan\n\
        .br\n    \
        green\n\
        .br\n    \
        magenta\n\
        .br\n    \
        red\n\
        .br\n    \
        white\n\
        .br\n    \
        yellow\n\
        .br\n    \
        Any number from 0-255 representing a 8-bit color\n\
        .br\n    \
        Any hex number representing a 24-bit color\n\n\
        .SH EXAMPLES\n\
        oximatrix -c red,white,blue\n\
        .br\n\
        oximatrix -c gs -r 20\n\n\
        .SH SEE ALSO\n\
        oxipipes(1)\n\
        .br\n\
        gol(1)\n\
        .br\n\
        2048(1)\n\
        .br\n\
        lant(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );
    fs::create_dir("../man").unwrap_or(());
    fs::remove_file("../man/oximatrix.1").unwrap_or(());
    fs::write("../man/oximatrix.1", manpage).unwrap();
}