use cursive::{
    Printer, Vec2,
    theme::{BaseColor, Color, ColorStyle},
    traits::View,
};
use rand::{
    Rng,
    seq::SliceRandom
};

const BASIC_COLORS: [BaseColor; 8] = [
    BaseColor::Black,
    BaseColor::Blue,
    BaseColor::Cyan,
    BaseColor::Green,
    BaseColor::Magenta,
    BaseColor::Red,
    BaseColor::White,
    BaseColor::Yellow
];

// represents the matrix display
pub struct MatrixView {
    pub width: usize,
    pub height: usize,
    droplets: Vec<Droplet>,
    colors: Vec<String>,
    max_drops: usize
}

impl View for MatrixView {
    fn draw(&self, printer: &Printer) {
        for droplet in &self.droplets {
            droplet.print(printer);
        }
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x;
        self.height = constraint.y;
        constraint
    }
}

impl MatrixView {
    pub fn new(colors: Vec<String>, max_drops: usize) -> MatrixView {
        MatrixView {
            width: 0,
            height: 0,
            droplets: Vec::new(),
            colors,
            max_drops
        }
    }

    pub fn update(&mut self) {
        self.droplets.retain(|droplet| droplet.tail_y <= self.height as isize);
        for droplet in &mut self.droplets {
            droplet.flicker = false;
            droplet.fall();
        }

        if self.droplets.len() <= self.max_drops {
            self.add_droplet();
        }

        let mut rand = rand::thread_rng();
        for droplet in &mut self.droplets {
            if rand.gen_range(0..30) == 0 {
                droplet.flicker = true;
            }
        }
    }

    fn add_droplet(&mut self) {
        if self.width == 0 || self.height == 0 { return; }
        let mut rand = rand::thread_rng();
        let x = rand.gen_range(0..self.width);
        let length = rand.gen_range(20..self.height * 2);
        let color = self.colors.choose(&mut rand).unwrap();
        self.droplets.push(Droplet::new(x, length, color));
    }
}

struct Droplet {
    x: usize,
    y: usize,
    tail_y: isize,
    chars: String,
    color: Color,
    flicker: bool
}

impl Droplet {
    fn print(&self, printer: &Printer) {
        for (i, c) in self.chars.chars().enumerate(){
            let style = ColorStyle::new(
                if self.flicker { Color::Light(BaseColor::White) } else { self.color },
                Color::TerminalDefault
            );
            if i > 0 {
                printer.with_color(style, |printer| {
                    if self.y as isize -i as isize >= 0 {
                        printer.print((self.x, self.y - i), &c.to_string());
                    }
                });
            }
            else {
                printer.print((self.x, self.y + i), &c.to_string());
            }
        }
    }

    fn new(x: usize, length: usize, color_str: &str) -> Droplet {
        Droplet {
            x,
            y: 0,
            tail_y: -(length as isize),
            chars: gen_rand_str(length),
            color: get_color(color_str),
            flicker: false
        }
    }

    fn fall(&mut self) {
        self.chars.insert(0, get_rand_char());
        self.chars.pop();
        self.y += 1;
        self.tail_y += 1;
    }
}

fn gen_rand_str(length: usize) -> String {
    std::iter::repeat(())
        .map(|_| get_rand_char())
        .take(length)
        .collect()
}

fn get_rand_char() -> char {
    let mut rand = rand::thread_rng();
    loop{
        let r = rand.gen_range(33..255);
        if r != 133 {
            return char::from(r);
        }
    }
}

pub fn get_color(color_str: &str) -> Color {
    let mut random =  rand::thread_rng();
    let mut basecolor =  BaseColor::Black;
    let mut is_base = true;
    let is_dark: bool = random.gen();
    // is the color a named color?
    match color_str.to_uppercase().as_str() {
        "GRAY" | "GS" | "GRAYSCALE" => {
            let a: u8 = random.gen();
            return Color::Rgb(a, a, a);
        },
        "MULTI" => return Color::Rgb(random.gen(),random.gen(),random.gen()),
        "MULTIB" => basecolor = *(BASIC_COLORS.choose(&mut random).unwrap()),
        "BLACK" => basecolor = BaseColor::Black,
        "BLUE" => basecolor = BaseColor::Blue,
        "CYAN" => basecolor = BaseColor::Cyan,
        "GREEN" => basecolor = BaseColor::Green,
        "MAGENTA" => basecolor = BaseColor::Magenta,
        "RED" => basecolor = BaseColor::Red,
        "WHITE" => basecolor = BaseColor::White,
        "YELLOW" => basecolor = BaseColor::Yellow,
        _ => is_base = false
    }

    if is_base {
        return if is_dark {
            Color::Dark(basecolor)
        }
        else {
            Color::Light(basecolor)
        }
    }

    // is it a number (0-255)?
    if let Ok(num) = color_str.parse::<u8>() {
        return Color::from_256colors(num);
    }

    // is it a hex string?
    if let Ok(cols) = hex::decode(color_str) {
        return Color::Rgb(cols[0], cols[1], cols[2])
    }

    Color::TerminalDefault
}
