use std::env;
use cursive::{
    event::Event,
    theme::{Color, BaseColor, PaletteColor, Theme},
    traits::Nameable
};
use crate::matrix::MatrixView;

mod matrix;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut num_drops: usize = 60;
    let mut colors: Vec<String> = vec!["GREEN".to_string()];
    for (i, arg) in args.iter().enumerate() {
        match arg.as_str() {
            "--color" | "-c" => {
                if i >= args.len() - 1 {
                    eprintln!("Please specify a color!");
                    return;
                }
                let c = &args[i + 1];
                let color_vec: Vec<String> = c.split(',').map(|color| color.to_string()).collect();
    
                for color in color_vec {
                    if matrix::get_color(&color) == Color::TerminalDefault {
                        eprintln!("Invalid color: {color}");
                        println!("Examples of valid colors:");
                        println!("The 8 TTY colors: black, blue, cyan, green, magenta, red, white, and yellow");
                        println!("0-255 representing any of the 256 colors");
                        println!("or a 6 digit hex code representing a color");
                        return;
                    }
                }
                colors = args[i + 1]
                    .split(',')
                    .map(|color_str| color_str.to_string())
                    .collect(); 
            },

            "--raindrops" | "-r" => {
                if i >= args.len() - 1 {
                    eprintln!("Invalid number!");
                    return;
                }
                else {
                    num_drops = match args[i + 1].parse() {
                        Ok(num) => num,
                        Err(_) => {
                            eprintln!("Invalid number!");
                            return;
                        }
                    };
                    if num_drops < 1 {
                        eprintln!("Number of raindrops must be 1 or more!");
                        return;
                    }
                }
            },

            _ => { }
        }
    }

    let mut theme = Theme::default();
    theme.palette[PaletteColor::Background] = Color::TerminalDefault;
    theme.palette[PaletteColor::View] = Color::TerminalDefault;
    theme.palette[PaletteColor::Primary] = Color::Light(BaseColor::White);
    let mut root = cursive::default();
    root.set_theme(theme);
    root.add_fullscreen_layer(MatrixView::new(colors, num_drops).with_name("matrix"));

    root.add_global_callback(Event::Refresh,|view|{
        let mut matrix = view.find_name::<MatrixView>("matrix").unwrap();
        matrix.update();
    });

    root.add_global_callback('q',|s| s.quit());
    root.set_fps(30);
    root.run();
}