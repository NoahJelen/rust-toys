#!/bin/sh
# This should theoretically run on any unix-like system (including macOS)
rm $HOME/.local/bin/{2048,gol,oxipipes,oximatrix,lant}
rm $HOME/.local/bin/share/man/man1/{2048.1,gol.1,oxipipes.1,oximatrix.1,lant.1}
cargo clean
cargo build --release
mkdir $HOME/.local/bin/share/man/man1/
cp target/release/oximatrix $HOME/.local/bin/oximatrix
cp target/release/2048 $HOME/.local/bin/2048
cp target/release/oxipipes $HOME/.local/bin/oxipipes
cp target/release/gol $HOME/.local/bin/gol
cp target/release/lant $HOME/.local/bin/lant
cp -r man/* $HOME/.local/bin/share/man/man1/
echo "Installed Rust Toys."
echo "make sure $HOME/.local/bin/ is in your \$PATH variable!"